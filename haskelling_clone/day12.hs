getInput :: String -> [Command]
getInput = (map sep) . lines
  where sep (d:rest) = (,) d (read rest)

type Heading = (Int,Int)
type Position = (Int,Int)
type Command = (Char,Int)

dir 'N' = ( 1, 0); dir 'S' = (-1, 0) ;
dir 'E' = ( 0, 1); dir 'W' = ( 0,-1)

addT :: (Num a, Num b) => (a,b) -> (a,b) -> (a,b)
addT (x,y) (a,b) = (,) (x+a) (y+b)

sm a (x,y) = (,) (x*a) (y*a)

right_once (x,y) = (-y,x)
rotate 0 = id 
rotate n = right_once . rotate ((n-1) `rem` 4)

quarters = (`div` 90)

changePos' :: Command -> (Heading,Position) -> (Heading,Position)
changePos' (cardinal,amount) (h,p) 
  | cardinal `elem` "NSEW" = (,) (addT h $ amount `sm`(dir cardinal)) p
changePos' ('R',amount) (h,p) = (turn h, p)
  where turn = rotate $ quarters amount
changePos' ('L',amount) (h,p) = (turn h, p)
  where turn = rotate $ negate $ quarters amount
changePos' ('F',amount) (h,p) = (h, addT p $ amount `sm` h)

go' :: [Command] -> (Heading, Position)
go' = foldr changePos' ((,) (1,10) (0,0)) . reverse

manhattan :: (a,Position) -> Int
manhattan (_,(x,y)) = abs x + abs y

main = interact (flip (++) "\n" . show . manhattan . go' . getInput)