pairSumsTo n (x:xs) = if (n-x) `elem` xs then Just (x,n-x) else pairSumsTo n xs
pairSumsTo _ [] = Nothing

input = (map read) .lines

multiply (Just (x,y)) = show $ x*y
multiply Nothing = "Nothing"

main= interact ((++ "\n"). show . multiply. (pairSumsTo 2020) . input)
-- main= interact ((++ "\n").show . f . input)


