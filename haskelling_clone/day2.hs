import Common

p :: Parser (Int, Int, Char, String)
p = do
  low <- many1 digit
  char '-'
  high <- many1 digit
  char ' '
  c <- letter
  string ": "
  s <- many1 letter
  return (read low, read high, c, s)

count c = length . filter (==c)

valid1 (low,high,target,string) = (amount >= low) && (amount <= high)
    where amount = count target string

main1 = interact (show .count True . map valid1 . map (useParser p) . lines)

main = main1