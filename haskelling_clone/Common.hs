module Common (module Text.Parsec, Parser, parse, useParser, count) where
{- HLINT ignore "Use camelCase" -}
import Data.List
import Data.Maybe

import Text.Parsec hiding(count, parse, uncons)
import qualified Text.Parsec as Parsec

-- |
-- >>> splitAtElem 5 [1..10]
-- [[1,2,3,4],[6,7,8,9,10]]
-- >>> splitAtElem 3 [2..4]
-- [[2],[4]]
-- >>> splitAtElem 5 [2..4]
-- [[2,3,4]]
-- >>> splitAtElem 'a' "babacas"
-- ["b","b","c","s"]

splitAtElem :: Eq a => a -> [a] -> [[a]]
splitAtElem _ [] = []
splitAtElem elem list = (take n list) : splitAtElem elem (drop (n+1) list)
    where maybePos = elemIndex elem list
          Just a = maybePos
          n = if isNothing maybePos
              then (length list) + 3
              else a

xor :: Bool -> Bool -> Bool
xor True False = True
xor False True = True
xor _     _    = False
-- podia ser /=

type Parser = Parsec String ()

parse :: Parser a -> String -> Either ParseError a
parse p = Parsec.parse p ""

p_test :: Parser (Int, String)
p_test = do
    h <- many1 digit
    char ' '
    g <- many1 anyChar
    return (read h, g)

-- |
-- >>> parse p_test "123 sinfonia"
-- Right (123,"sinfonia")
-- >>> parse p_test "sinfonia"
-- Left (line 1, column 1):
-- unexpected "s"
-- expecting digit


p_test2 :: Parser (Int, String)
p_test2 = do
    string "numero: "
    h <- many1 digit
    char ' '
    string "rua: "
    g <- many1 anyChar
    return (read h, g)

-- |
-- >>> parse p_test2 "numero: 123 rua: Sinfonia Branca"
-- Right (123,"Sinfonia Branca")

useParser p text = res
    where Right res = parse p text
          
-- |
-- >>> useParser p_test2 "numero: 2 rua: Largo"
-- (2,"Largo")

count c = length . filter (==c)