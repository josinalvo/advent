
pairSumsTo n (x:xs) = if (n-x) `elem` xs then Just (x,n-x) else pairSumsTo n xs
pairSumsTo _ [] = Nothing

tripleSumsTo n (x:xs) = case pairSumsTo (n-x) xs of
                        (Just(a,b)) -> (a,b,x)
                        Nothing -> tripleSumsTo n xs
input :: String -> [Integer]
input = (map read) .lines

multiply (x,y,z) = x*y*z

main= interact ((++ "\n"). show . multiply. (tripleSumsTo 2020) . input)
-- main= interact ((++ "\n").show . f . input)


