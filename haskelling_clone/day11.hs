import qualified Data.Vector as V
import Data.Vector (imap, (!?))
import Data.Maybe ( catMaybes )

ltov = V.fromList 
vtol = V.toList 

step :: V.Vector (V.Vector Char) -> V.Vector (V.Vector Char)
step room = imap (\y line -> imap (\x val -> stepP x y val) line) room
   where direcs = [(0,1),(1,0),(0,-1),(-1,0),(1,1),(-1,-1),(1,-1),(-1,1)]
         stepP x y v = case v of
                'L' -> if used neighs == 0 then '#' else 'L'
                '#' -> if used neighs >= 4 then 'L' else '#'
                '.' -> '.'
             where chkDir (x1,y1) = do
                            line <- room !? (y+y1)
                            line !? (x+x1)
                   neighs = catMaybes $ map chkDir direcs
                   used = length . filter (=='#')

input :: String -> V.Vector (V.Vector Char)
input s = ltov $ map ltov $ lines s


vtol_matrix :: V.Vector (V.Vector a) -> [[a]]
vtol_matrix m = vtol $ V.map vtol $ m

converge :: Eq a => (a -> a) -> a -> a
converge f m = if m == new then m else converge f new
        where new = f m

count e l = length $ filter (==e) $ l

maint = interact (unlines . map show .  vtol_matrix . (converge step) . input)


main = interact (show . sum . map (count '#') .  vtol_matrix . (converge step) . input)


