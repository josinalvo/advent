import Data.Char
main = do
    input <- getContents -- le do stdin. Ou vc digita a entrada, ou faz cat entrada | runhaskell teste.hs
    let l = map toUpper input
        j = lines l -- quebra a string em uma lista de strings, usando o \n como delimitador e excluindo o \n
        k = (filter (elem 'C')) j
    (putStrLn . show) (l,j,k, (unlines k))
    print (l,j,k, (unlines k)) -- da na mesma que a linha acima
    
