module DayTwo where

import Data.List
import Data.Maybe


-- |
-- >>> splitAtElem 5 [1..10]
-- ([1,2,3,4],[6,7,8,9,10])
-- >>> splitAtElem 3 [2..4]
-- ([2],[4])
-- >>> splitAtElem 5 [2..4]
-- ([2,3,4],[])

splitAtElem elem list = (,) (take n list) (drop (n+1) list)
    where maybePos = elemIndex elem list
          Just a = maybePos
          n = if isNothing maybePos
              then (length list) + 3
              else a

xor True False = True
xor False True = True
xor _     _    = False


valid password = (count >= (read start)) && (count <= (read finish))
     where (start,finish) = (splitAtElem '-') . (!!0) $ password
           character = (!!0) . (!!1) $ password
           count = length . filter (== character) . (!!2) $ password

valid2 password = (c1 == character) `xor` (c2 == character)
    where (start,finish) = (splitAtElem '-') . (!!0) $ password
          character = (!!0) . (!!1) $ password
          actual_pass = password !! 2
          c1 = actual_pass !! (read start - 1)
          c2 = actual_pass !! (read finish - 1)

main = do
    a <- getContents
    let passwords = (filter valid2) . (map words) . lines $ a
    putStrLn . show . length $ passwords