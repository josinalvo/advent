{- HLINT ignore "Eta reduce" -}
{- HLINT ignore "Use camelCase" -}

line_cost pos line = if line !! pos == '#'
                     then 1
                     else 0

lines_cost :: Int -> Int -> [String] -> Int
lines_cost _ _ [] = 0
lines_cost add pos (line:lines) = curr_cost + rest
       where line_size = length line
             rest = lines_cost add (rem (pos+add) line_size) lines
             curr_cost = line_cost pos line

f x = (+1) x

evens [x,_] = [x]
evens [x] = [x]
evens [] = []
evens (x:_:zs) = x:(evens zs)


main = do
    a <- getContents
    let ans = product [lines_cost i 0 (lines a)| i<-[1,3,5,7]]
        ans2 = lines_cost 1 0 (evens . lines $ a)
    print (ans * ans2)
