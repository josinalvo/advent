import Common

import Text.Parsec hiding(count, parse, uncons)

import Data.Maybe
import Data.List
import Data.Map ((!),(!?), fromList, toList)
--TODO achava que isso viria com o import Common

-- readBag :: Num a => String -> (String, Maybe [(a,String)])
-- readBag s = undefined
--     where [bag, contentsString] = (map unwords) . (splitAtElem "contain") . words $ s
--           unbag bagString = head . (map unwords) . (splitAtElem "bags") . words $ bagString
--           contents = if contentsString == "no other bags"
--                      then Nothing
--                      else (map unbag) . splitAtElem ", " $ contentsString
--           contentToTuple s = undefined


allBags :: String -> [(String, [(Int, String)])]
allBags = map (useParser bagLine) . lines

bagLine :: Parser (String, [(Int,String)])
bagLine = do
    g <- manyTill anyChar (try $ string " bags contain ")
    
    h <- (try noBags) <|> sepBy bagFromList (string ", ")
    char '.'
    return (g,h)

bagFromList :: Parser (Int,String)
bagFromList = do
    amount <- many1 digit
    char ' '
    --description <- many1 (noneOf ".,")
    description <- manyTill anyChar (try $ string " bag")
    many (char 's') --TODO should be 0 or 1
    return (read amount, description)

noBags:: Parser [(Int,String)]
noBags = do
    string "no other bags"
    return []

-- |
-- >>> useParser bagLine "mirrored tomato bags contain 3 faded maroon bags, 3 dark green bags."
-- ("mirrored tomato",[(3,"faded maroon"),(3,"dark green")])
-- >>> useParser bagLine "posh lime bags contain 1 dim lavender bag."
-- ("posh lime",[(1,"dim lavender")])
-- >>> useParser bagLine "shiny plum bags contain no other bags."
-- ("shiny plum",[])
-- >>> allBags $ unlines [ex_1,ex_2,ex_3]
-- [("mirrored tomato",[(3,"faded maroon"),(3,"dark green")]),("posh lime",[(1,"dim lavender")]),("shiny plum",[])]

ex_1 = "mirrored tomato bags contain 3 faded maroon bags, 3 dark green bags."
ex_2 = "posh lime bags contain 1 dim lavender bag."
ex_3 = "shiny plum bags contain no other bags."

-- |
-- >>> bags = allBags $ unlines [ex_1,ex_2,ex_3]
-- >>> leadsTo bags "mirrored tomato" "faded maroon"
-- True
-- >>> leadsTo bags "posh lime" "faded maroon"
-- False
-- >>> leadsTo bags "posh lime" "dim lavender"
-- True


leadsTo allBagsMap b1 b2 = b2 `elem` (map snd targetsFromB1)
    where targetsFromB1 = (!) allBagsMap b1

foundFrom allBagsMap x y = leadsTo allBagsMap y x

reversedMap allBags = fromList . (map group_to_relation) $ groups
    where allBagStrings = map fst allBags
          allBagsMap = fromList allBags
          foundFrom' = foundFrom allBagsMap
          pairs = [(b1,b2) | b1 <- allBagStrings, b2 <- allBagStrings, foundFrom' b1 b2]
          groups = groupBy (\ x y -> fst x == fst y) $ sortOn fst $ pairs
          group_to_relation listOfPairs = (,) (fst . head $ listOfPairs) (map snd listOfPairs)


directMap allBags = fromList allBags

contents bagName bagMap = count
    where search1 = bagMap !? bagName
          Just search2 = search1
          search_ready = if isNothing search1
                         then []
                         else search2
          count = foldr addBagContrib 0 search_ready
            where addBagContrib (qtd,bagName') count = count + qtd + qtd*(contents bagName' bagMap)

-- allPathsToRome ::  String -> [(String, [(Int, String)])] -> [String]
-- todo remove IO
allPathsToRome start allBags = do
                   ans <- manyStep ([start],[])
                   return (snd ans)
    where bagMap = reversedMap allBags
          f bag (new_entering,found)= (,) (new_entering ++ justFound) (bag:found)
            where justFound = filter (bag /=) $ filter (`notElem` new_entering) $ filter (`notElem` found) $ possibleContainers bag
          step (entering, found)= foldr f ([],found) entering
          possibleContainers bag = ans 
              where query = bagMap !? bag
                    Just list = query
                    ans = if isNothing query
                          then []
                          else list
          manyStep :: ([String],[String]) -> IO ([String],[String])                
          manyStep (entering,found)= do
              (putStrLn . show) (entering,found)
              if entering == [] 
              then return (entering, found) 
              else do 
                   let next = step (entering,found) 
                   ans <- manyStep next
                   return ans



main1 :: IO ()
main1 = do
    text <- getContents
    allPaths <- (allPathsToRome "shiny gold")  . allBags $ text
    let groups = group $ sort allPaths
    print $ (length groups)-1
    print $ filter ((> 1) . length) groups -- TODO solve this bug
    
main = interact (show . (contents "shiny gold") . directMap . allBags)