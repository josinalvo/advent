module Main where

import Common
import Data.List


splitIntoGroups :: String -> [[String]]
splitIntoGroups = (splitAtElem "") . lines

a :: [Char]
a = ['a','b','c']

joinAll:: [[a]]-> [a]
joinAll  = foldr1 (++) 

-- | joinByAnd
-- >>> joinByAnd ["abcz","zbc","bza"]
-- "bz"

joinByAnd:: [String] -> String
joinByAnd allAns = [c| c <-['a'..'z'], all (c `elem`) allAns ]

countDistinct :: (Eq a, Ord a) => [a] -> Int
countDistinct = length . group . sort

-- |
-- >>> splitIntoGroups "abc\ndef\n\ng"
-- [["abc","def"],["g"]]

main :: IO ()
main = do
    input <- getContents
    print $ sum . (map countDistinct) . (map joinByAnd) . splitIntoGroups $ input