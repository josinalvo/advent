import Data.List
import Common

passport :: String -> [(String, String)]
passport string = map to_tuple pairs
    where pairs = words string

goodPassport :: String
goodPassport = "hcl:#ae17e1 iyr:2013 eyr:2024 ecl:brn pid:760753108 byr:1931 hgt:179cm"

keysFromPassportString :: String -> [String]
keysFromPassportString p = sort $ map fst $ passport p

goodKeys :: [String]
goodKeys = keysFromPassportString goodPassport

perfectPassport :: String
perfectPassport = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd byr:1937 iyr:2017 cid:147 hgt:183cm"

perfectKeys :: [String]
perfectKeys = keysFromPassportString perfectPassport

to_tuple :: String -> (String, String)
to_tuple pair = (key,value)
    where [key,value] = splitOn ':' pair

passportStrings :: String -> [String]
passportStrings bigString = clean . listOfStringsToString . notempty $ eachPassIsListOfStrings
   where joinSpace x y = x ++ " " ++ y
         clean = (filter (/= "\n")) . (filter (/= ""))
         notempty = (filter (/= []))
         listOfStringsToString = (map (foldr1 joinSpace))
         eachPassIsListOfStrings = (splitOn "") . lines $ bigString

val :: Eq t => t -> [(t, String)] -> String
val _ [] = "invalid for all verifications"
val key (pair:rest)
   | (fst pair == key) = (snd pair)
   | otherwise = val key rest

hcl_ok' :: String -> Bool
hcl_ok' ('#':rest) = all (`elem` colorchar) rest
hcl_ok' _ = False

pid_ok' :: String -> Bool
pid_ok' pid = and $ [length pid == 9] ++ map (`elem` ['0'..'9']) pid



colorchar :: [Char]
colorchar = ['0'..'9']++['a'..'f']

goodPassportStrings :: String -> [String]
goodPassportStrings s = ((filter acceptable). passportStrings) s
    where good p = (keysFromPassportString p) == goodKeys
          perf p = (keysFromPassportString p) == perfectKeys
          byr_ok p = (val "byr" (passport p)) `elem` (map show) [1920..2002]
          iyr_ok p = (val "iyr" (passport p)) `elem` (map show) [2010..2020]
          eyr_ok p = (val "eyr" (passport p)) `elem` (map show) [2020..2030]
          hgt_ok p = (val "hgt" (passport p)) `elem` [(show i)++"cm" | i <-[150..193]] ++ [(show i)++"in" | i <-[59..76]]
          hcl_ok p = hcl_ok' (val "hcl" (passport p))
          ecl_ok p = (val "ecl" (passport p)) `elem` words "amb blu brn gry grn hzl oth"
          pid_ok p = pid_ok' (val "pid" (passport p))
          acceptable p = ((good p) || (perf p)) && fields_ok p
          fields_ok p = and $ [byr_ok, iyr_ok, hcl_ok, eyr_ok, hgt_ok, ecl_ok, pid_ok] <*> [p]
splitOn :: Eq a => a -> [a] -> [[a]]
splitOn = splitAtElem
main = do
       input <- getContents
       print "total"
       (print . length . passportStrings) input
       print "bons"
       (print . length . goodPassportStrings) input