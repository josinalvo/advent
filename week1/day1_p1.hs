p1 = do 
    a <- getContents
    let nums  = (map read) . lines $ a :: [Int]
        pairs = [(i,j) | i <- nums, j <- nums]
        cools = filter (\ (a,b) -> a+b == 2020) pairs
    putStrLn . show . (uncurry (*)) . (!!0) $ cools 
    

main = do 
    a <- getContents
    let nums  = (map read) . lines $ a :: [Int]
        triples = [[i,j,k] | i <- nums, j <- nums, k <- nums]
        cools = filter ((==2020) . sum) triples
        --cools = zip triples $ map sum triples
    -- putStrLn . show . (uncurry (*)) . (!!0) $ cools 
    putStrLn . show . product . (!!0) $ cools
