module Seats where

-- |
-- >>> toSeatId "BFFFBBFRRR"
-- 567
-- >>> toSeatId "FFFBBBFRRR"
-- 119

toSeatId :: Num a => [Char] -> a
toSeatId [] = 0
toSeatId (c:cs) = (toNum c) * powerOfTwo cs + toSeatId cs
    where toNum c = if c `elem` "BR" then 1 else 0
          powerOfTwo cs = 2^(length cs)

mySeat :: (Num a, Eq a, Enum a) => [a] -> a
mySeat givenSeats = (+1) $ head [i | i <- givenSeats, (not $ (i+1) `elem` givenSeats), (i+2 `elem` givenSeats)]

main_1 :: IO ()
main_1 = do
    input <- getContents
    (print . maximum . (map toSeatId) . lines) input

main :: IO ()
main = do
    input <- getContents
    (print . mySeat . (map toSeatId) . lines) input
