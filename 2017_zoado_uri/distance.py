reserv = {}

def dist_aux(s,t):
    if (len(s) == 0 and len(t) == 0):
        return 0
    if (len(s) == 0):
        return len(t)
    if (len(t) == 0):
        return len(s)
    if (len(s),len(t)) in reserv:
        return reserv[(len(s),len(t))]
    s_prime = s[:len(s)-1]
    t_prime = t[:len(t)-1]
    if s[len(s)-1] == t[len(t)-1]:
        ans = dist_aux(s_prime,t_prime)
        reserv[(len(s),len(t))] = ans
        return ans
    else:
        replace = dist_aux(s_prime,t_prime)+1
        delete  = dist_aux(s_prime,t)+1
        insert  = dist_aux(s,t_prime)+1
        ans = min ([replace,delete,insert])
        reserv[(len(s),len(t))] = ans
        return ans
    
    
def dist(s,t):
    global reserv
    reserv = {}
    return dist_aux(s,t)

def main():
    start = input()
    max_size = int(input())
    strings = []
    dists = []
    for i in [0,1,2,3,4]:
        string = input()
        strings.append(string)
        dists.append(dist(start,string))
    if min (dists) > max_size:
        print(-1)
    else:
        print(dists.index(min(dists))+1)
        print(min(dists))
    #print ((start, max_size, strings))

main()
