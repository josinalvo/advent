layer1 = {}
layer2 = {}
layer3 = {}

def check(len_s,len_t):
    if (len_s == 0 and len_t == 0):
        return 0
    if (len_s == 0):
        return len_t
    if (len_t == 0):
        return len_s
    for l in [layer1,layer2,layer3]:
        if (len_s,len_t) in l:
            return l[(len_s,len_t)]
    assert(False)

def dist_aux(len_s,len_t,last_s,last_t):
    if (len_s == 0 and len_t == 0):
        return 0
    if (len_s == 0):
        return len_t
    if (len_t == 0):
        return len_s
    len_s_prime, len_t_prime = len_s-1, len_t-1
    if last_s == last_t:
        ans = check(len_s_prime,len_t_prime)
        layer3[(len_s,len_t)] = ans
        return ans
    else:
        replace = check(len_s_prime,len_t_prime)+1
        delete  = check(len_s_prime,len_t)+1
        insert  = check(len_s,len_t_prime)+1
        ans = min ([replace,delete,insert])
        layer3[(len_s,len_t)] = ans
        return ans

def too_big(max_len):
    ok = (lambda a: a <= max_len)
    for l in layer1, layer2, layer3:
        if filter(ok,l.values()) != []:
            return False
    return True
    
def dist(s,t,max_len=10**9):
    global layer1, layer2, layer3
    if (abs(len(s)-len(t)) > max_len):
        return max_len+1
    for n in range(0,len(s)+len(t)+1):
        if too_big(max_len):
            return max_len+1
        for size_s in range(n):
            size_t = n-size_s
            if (size_t > len(t) or size_s > len(s)):
                continue
            last_s, last_t = s[size_s-1],t[size_t-1]
            # print(size_s,n-size_s)
            dist_aux(size_s,size_t,last_s,last_t)
        # print("----")
        (layer1,layer2,layer3) = (layer2,layer3,{})
    return check(len(s),len(t))

def main():
    start = input()
    max_len = int(input())
    strings = []
    dists = []
    for i in [0,1,2,3,4]:
        string = input()
        strings.append(string)
        dists.append(dist(start,string,max_len))
    if min (dists) > max_len:
        print(-1)
    else:
        print(dists.index(min(dists))+1)
        print(min(dists))
    # print (layer1)
    # print (layer2)
    # print (layer3)

main()
#dist("aab","bb")
