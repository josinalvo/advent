module Fib where
import Test.DocTest

-- $setup
-- >>> import Control.Applicative
-- >>> import Test.QuickCheck
-- >>> newtype Small = Small Int deriving Show
-- >>> instance Arbitrary Small where arbitrary = fmap (Small . (`mod` 10)) arbitrary

-- | Compute Fibonacci numbers
--
-- The following property holds:
--
-- prop> \(Small n) -> fib n == fib (n + 2) - fib (n + 1)
fib :: Int -> Int
fib 0 = 0
fib 1 = 1
fib n = fib (n - 1) + fib (n - 2)


-- The following property does not hold:
--
-- prop> \(Small n, Small m) -> n == 0 || m `rem` n == m `mod` n
-- prop> \(Small n, Small m) -> n == 0 || (negate m) `rem` n == (negate m) `mod` n

-- div of divMod has a bias to negative infinity.
-- >>>(-2) `div` 4 
--  -1, 
-- >>>(-2) `mod` 4 
-- 2

-- rem is better to keep negative
tests = doctest ["--verbose","./quickdoctest.hs"]

main = tests