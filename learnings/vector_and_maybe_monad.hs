import Data.Vector
import Test.DocTest

a = fromList [1,2,3] :: Vector Int

test = do
    v1 <- a !? 0
    v2 <- a !? 2
    return $ v1 + v2

test_n = do
    v1 <- a !? 3
    v2 <- a !? 2
    return $ v1 + v2


-- $
-- >>> test
-- Just 4
-- >>> test_n
-- Nothing

-- $ for repetitions
-- >>> a = iterate (+10) 0
-- >>> a !! 3
-- 30

tests = doctest ["--verbose","./vector_and_maybe_monad.hs"]

main = tests