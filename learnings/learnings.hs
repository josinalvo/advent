-- a =    case f (2020 - x) xs of
--               Nothing -> f' xs
--               Just y  -> y * x

import Data.Bool
--import Data.Vector

f = (bool "not vowel" "vowel") . ( `elem` "aeioi")

-- day 11
-- the maybe monad
-- imap gets indexes as well (vector)
-- mapMaybe == map . catMaybes ??
-- converge as a separate function

-- $
-- bool 1 2 True
-- 2
-- >>> f 'd'
-- "not vowel"
-- >>> f 'e'
-- "vowel"

-- day 12
-- rotn 0 = id
-- rotn n = rot . rodn (n-1 -- mod, rem to avoid excess ops and negatives)

main = print 12