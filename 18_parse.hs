-- not working yet

import qualified Text.Parsec as Parsec
import Text.Parsec ( (<|>) )

data ExpT = Leaf { val :: Int } | Ope { ope :: Char, left :: ExpT, right :: ExpT } deriving Show

leaf :: Parsec.Parsec String () ExpT
leaf = do
    a <- Parsec.many Parsec.digit
    return $ Leaf (read a)

opeNode :: Parsec.Parsec String () Char
opeNode = Parsec.oneOf "*+"
    
simpleExp :: Parsec.Parsec String () ExpT
simpleExp = do
    l1 <- leaf
    Parsec.spaces
    o <- opeNode 
    Parsec.spaces
    pure (Ope o l1) <*> leaf

a = Parsec.parse simpleExp "()" "123*3"

-- |
-- >>> a
-- Right (Ope {ope = '*', left = Leaf {val = 123}, right = Leaf {val = 3}})


expr :: Parsec.Parsec String () ExpT
expr = do
    l1 <- leaf
    Parsec.spaces
    o <- opeNode 
    Parsec.spaces
    l2 <- Parsec.try expr <|> leaf
    return $ Ope o l1 l2

b = Parsec.parse expr "()" "123*3"
c = Parsec.parse expr "()" "56+3*100"
-- |
-- >>> b
-- Right (Ope {ope = '*', left = Leaf {val = 123}, right = Leaf {val = 3}})
-- >>> c
-- Right...

-- data ExpT = Leaf { val :: Int } | Ope { ope :: Char, left :: ExpT, right :: ExpT } deriving Show

eval :: ExpT -> Int
eval (Leaf n) = n
eval (Ope '+' exp1 exp2) = (+) (eval exp1) (eval exp2)
eval (Ope '*' exp1 exp2) = (*) (eval exp1) (eval exp2)

-- |
-- >>> Right resultTree = c
-- >>> eval  resultTree
-- 356

-- the tree shows a bias that was unwanted :(

main = print 12