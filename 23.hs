-- runs part 1 ok, part2 maybe works, but is too slow

{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
import qualified Data.Vector  as V
import Data.List (sortOn)
import Data.Char ( digitToInt )
data Cup = Cup {label :: Int, nextPosition :: Int} deriving Show

type Start = Int

data Game = Game {cups :: (V.Vector Cup), start :: Int}

instance Show Game
    where show g@(Game cups start) = showFst g ++ show' (step1 g)
           where show' g@(Game cups start_local) 
                  | (start_local == start) = ""
                  | otherwise = showFst g ++ show' (step1 g)
                 fst_cup (Game cups start)= cups V.! start
                 showFst = show . label . fst_cup
                 step1 (Game c s) = Game c (nextPosition $ c V.! s)

example :: [Int]
example = map digitToInt "389125467"

fillup :: (Num a, Enum a) => [a] -> [a]
fillup l = l ++ [10..10^6]

input :: [Int]
input = map digitToInt "135468729"

shift1 :: [a] -> [a]
shift1 list = take (length list) $ drop 1 $ cycle list

toCupVector :: [Int] -> Game
toCupVector labels = Game vecOfCups posFirst
    where toListOfStrings = map (:[])
          next_addrs = map (+ negate 1) $ shift1 labels
          listOfCups = zipWith Cup labels next_addrs
          vecOfCups = V.fromList $ sortOn label listOfCups
          posFirst = label (head listOfCups) - 1

stepGame :: Game -> Game
stepGame (Game list startP) = Game (list `V.update` vec_of_changes) new_start_pos
    where start = list V.! startP
          next c = list V.! nextPosition c
          (r1,r2,r3) = (next start, next r1, next r2)
          new_start_pos = nextPosition r3
          size = length list
          backward_from query = if previous `elem` labels_removed
                                then backward_from previous
                                else previous
                    where labels_removed = map label [r1,r2,r3]
                          previous = backward1 query
                          backward1 label = if label - 1 == 0
                                            then max_label 
                                            else label - 1
                          max_label = length list
          vec_of_changes = V.fromList rep_pairs
            where insertAfter = list V.! (backward_from (label start) - 1)
                  positionOfCup c = label c - 1
                  replacement_old_start = Cup (label start) new_start_pos
                  replacement_r3 = Cup (label r3) (nextPosition insertAfter)
                  replacement_insertAfter = Cup (label insertAfter) (positionOfCup r1)
                  reps = [replacement_old_start, replacement_r3, replacement_insertAfter]
                  rep_pairs = zip (map positionOfCup reps) reps

iter _ val 0 = val
iter f val n = iter f (f val) (n - 1)

ans_part2 (Game cups start) = (label cup2) * (label cup3)
    where cup1 = cups V.! 0
          next c = cups V.! nextPosition c
          (cup2,cup3) = (next cup1, next cup2) 


part2 :: IO ()
part2 = print $ ans_part2 $ iter stepGame (toCupVector $ fillup input) 1 

main :: IO ()
main = part2

part1 = iter stepGame (toCupVector input) 100 
-- did work once, idk if works now