#define MAX 30000001
#include <stdio.h>
int arr[MAX];

int start[10] = {2,3,1};
int size_start = 3;

// int start[10] = {20,0,1,11,6,3};
// int size_start = 6;

void clear() {
      for (int i = 0; i < MAX; i++)
          arr[i] = -1;
}

void populate_with_start() {
    for (int i = 0 ; i <= size_start-2; i++) {
        int element = start[i];
        arr[element] = i;
    }
}

int main() {
    int next_pos = size_start - 1;
    int last = start[next_pos];
    //and arr is the vector for control of taken positions
    clear();
    populate_with_start();
    int goal = 30*1000*1000;
    //goal = 2020;
    while (next_pos < goal-1) { //this -1 is an enigma to me
        int last_time = arr[last];
        arr[last] = next_pos;
        if (last_time == -1) {
            
            last = 0;
        }
        else {
            int diff = next_pos - last_time;
            last = diff;
        }
        next_pos+=1;
    }
    printf("%d\n",last);
}
