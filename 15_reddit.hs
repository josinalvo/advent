-- nix-shell -p 'haskellPackages.ghcWithPackages (pkgs: [pkgs.vector])' --run 'ghc Test.hs; tim e ./Test'
-- [1 of 1] Compiling Main             ( Test.hs, Test.o )
-- Linking Test ...
-- Just 175594
-- 
-- real    0m5.085s
-- user    0m5.013s
-- sys     0m0.072s

{-# LANGUAGE BangPatterns #-}

import           Control.Monad               (forM_)
import           Control.Monad.ST
import           Data.Foldable               (foldlM)
import           Data.List.NonEmpty          (NonEmpty (..))
import qualified Data.List.NonEmpty          as NE
import qualified Data.Vector.Unboxed.Mutable as VM

run :: NonEmpty Int -> Int -> Maybe Int
run input target
    | target <= 0            = Nothing
    | target <= length input = Just $ input NE.!! (target - 1)
    | otherwise = let len = length input
                   in Just $ runST $ do
        { v <- VM.replicate (maximum (target : NE.toList input) + 1) 0
        ; forM_ (zip (NE.init input) [1..]) $ uncurry (VM.write v)
        ; foldlM (speakNum v) (NE.last input) [len..target-1]
        }

speakNum :: VM.MVector s Int -> Int -> Int -> ST s Int
speakNum !v !prev i = do
    { prevPos <- VM.unsafeRead v prev
    ; VM.write v prev i
    ; return $ if prevPos == 0 then 0 else i - prevPos
    }

main = putStrLn . show $ run (0:|[3,6]) 30000000

