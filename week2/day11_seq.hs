import Data.Sequence

readIt = lines

positions :: [[Char]] -> [[(Int, Int)]]
positions lines = positions' 0 lines
    where positions' ystart (_:lines) = (map (flip (,) $ ystart) [0..last_elem_line]):positions' (ystart+1) lines
          positions' _ [] = []
          last_elem_line = Prelude.length (head lines) - 1

possibleAdds = Prelude.filter (/= (0,0)) $ pure( \x y -> (x,y) ) <*> [-1,0,1] <*> [-1,0,1]

-- posAndAdjs1 seats (posx, posy) = (,) curr_pos adjs
--     where addList (a,b) (c,d) = (a+c,b+d)
--           possiblePos :: [(Int,Int)]
--           possiblePos = map (addList (posx,posy)) possibleAdds
--           atPos :: [[Char]] -> (Int,Int) -> [Char]
--           atPos seats (x,y) = [seats!!y!!x | not(x > max_x || x < 0 || y > max_y || y < 0)]
--           -- lista com 0 ou 1 elementos -- tentar com maybe
--           (max_x, max_y) = ((length $ head seats)-1  , length seats -1 )
--           adjs = foldr1 (++) $ map (atPos seats) possiblePos
--           curr_pos = head $ atPos seats (posx,posy)


posAndAdjs :: Seq (Seq Char) -> (Int, Int) -> (Char, [Char])
posAndAdjs seats (posx, posy) = (,) curr_pos adjs
    where seek add = seek' add 1
          seek' a@(add_x,add_y) n = if found == ['.']
                                    then seek' a (n+1)
                                    else found
              where newPos = (,) (posx+add_x*n) (posy+add_y*n)
                    found = atPos seats newPos
          atPos seats (x,y) = [seats `index` y  `index`x | not(x > max_x || x < 0 || y > max_y || y < 0)]
          -- lista com 0 ou 1 elementos -- tentar com maybe
          (max_x, max_y) = ((Data.Sequence.length $ index seats 0)-1  , Data.Sequence.length seats -1 )
          adjs = foldr1 (++) $ map seek possibleAdds
          curr_pos = head $ atPos seats (posx,posy)


allAdjs seats = map  adjsOfLine (positions seats)
   where adjsOfLine = map (posAndAdjs seatsSeq) 
         seatsSeq = fromList $ map fromList seats

nextSeats seats = map newLine adjs
    where adjs = allAdjs seats
          next ('.',_) = '.'
          next ('L',list) = if occupied list == 0
                            then '#'
                            else 'L'
          next ('#',list) = if occupied list >= 5
                            then 'L'
                            else '#'
          newLine line = map next line

occupied list = Prelude.length $ Prelude.filter (== '#') list

-- build grid of ()
-- pos -> adjs ([[Char]])
-- map adjs -> pos

converge seats = if next == seats
                 then seats
                 else converge next
    where next = nextSeats seats

main = interact (show . sum .(map occupied) . converge . readIt)

-- main = interact (unlines . (map show) . converge . readIt)
--main = interact (unlines . (map show) . (\ s -> adjsToOne s (2,3)) . readIt)
-- main = interact (unlines . (map show) . positions . readIt)