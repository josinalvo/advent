import Common

type Pos = Int
type Rem = Int

input :: [Char] -> [(Int,Pos)]
input string = times_list
    where [_,times] = lines string
          times_list =  map (\(x,y) -> (,) (read x) y) $ filter ((/= "x").fst) $ zip (splitAtElem ',' times) [0..]

clean :: (Int,Pos) -> (Int,Rem)
clean (x,y) = (x,time_before)
   where time_after = y
         smallest_time_after = time_after `rem` x
         time_before = if smallest_time_after == 0
                       then 0
                       else x-smallest_time_after

-- FIRST SOLUTION

multRem step start divisor remainder = head $ filter (\x-> (rem x divisor) == remainder) $ candidates
    where candidates = [start,start+step ..]

solve :: [(Int,Target)] -> Int
solve [(_,pos)] = pos
solve ((val,pos):others) = (multRem prod rest val pos) `rem` (prod*val)
    where rest = solve others
          prod = product $ map fst $ others


-- SECOND SOLUTION


euclid :: Integral t => t -> t -> [t]
euclid a 1 = [a,1]
euclid a 0 = [a,0]
euclid m n = m : euclid n (rem m n)

-- |
-- >>> euclid 10 4
-- [10,4,2,0]
--
-- >>> euclid 100 71
-- [100,71,29,13,3,1]

coprimes m n = (last euclidList == 1)
    where euclidList = euclid m n
{- HLINT ignore "Use camelCase" -}
coef_to_one m n
    | not $ coprimes m n = error "not findable" 
    | rem m n == 1 = (1, negate $ div m n)
    | otherwise = (ms_to_y*ys,ns_to_y*ys+ns)
        -- m n y
        where (ns, ys) = coef_to_one n (rem m n)
              (ms_to_y,ns_to_y) = (1, negate $ div m n)

-- |
-- >>> coef_to_one 19 13
-- (-2,3)
-- >>> (-2)*19+3*13
-- 1

type Target = Int
solveDio :: [(Int,Target)] -> Int
solveDio s = (sum ansTerms) `mod` bigProduct 
    where mods = map fst s
          targets = map snd s
          bigProduct = product mods
          complements = map (div bigProduct) mods
          toOnes = map fst $ map (uncurry coef_to_one) $ zip complements mods
          ansTerms = zipWith3 (\coef complement target -> coef*complement*target) toOnes complements targets
-- | see https://brilliant.org/wiki/chinese-remainder-theorem/
-- >>> solveDio [(3,1), (5,4), (7,6)]
-- 34

main = interact ((++ "\n") . show. solve. (map clean). input)
