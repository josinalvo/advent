{- HLINT ignore "Use camelCase" -}
import Common
import Data.List -- thougth tails already cage from Common :(

valid_n_plus_one :: (Eq a, Num a) => [a] -> Bool
valid_n_plus_one list = foundIn (last list) (init list)
    where allPossible list = [i+j | i <- list, j <- list, j /= i]
          foundIn e list = elem e $ allPossible list

allSetsOfInterest :: Int -> [a] -> [[a]]
allSetsOfInterest size list = notTooSmall $ maxSize $ allTails
    where allTails = tails list
          maxSize = map (take $ size+1)
          notTooSmall = filter ((==(size + 1)) . length)

firstBad :: Int -> [Int] -> Int
firstBad size = last . head . filter (not . valid_n_plus_one) . (allSetsOfInterest size)

searchSum :: [Int] -> Int ->  [Int]
searchSum list target = [minimum,maximum] <*> subSets
    where max = length list - 1
          subSets = [subSet i j| i <-[0..max],  j<-[(i+1)..max], sum (subSet i j) == target]
          subSet i j = (drop i) . (take $ j+1) $ list

-- |
-- >>> (i,j)=(5,7)
-- >>> (drop i) . (take $ j+1) $ [0..10]
-- [5,6,7]

main = interact  (show . sum . (searchSum <*> (firstBad 25)) . input)
    where input = (map read) . lines