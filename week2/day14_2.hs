import Common
import Data.Map (fromList, insert, toList, Map)

import Text.Parsec hiding(count, parse, uncons)


data PLine = ToMem {addr :: Integer, val :: Integer} | Mask String deriving Show
type BinaryString = String

memLine :: Parser (PLine)
memLine = do
    string "mem["
    addr <- many1 digit
    string "] = "
    val <- many1 digit
    many $ string "\n"
    return $ ToMem (read addr) (read val)

maskLine :: Parser (PLine)
maskLine = do
    string "mask = "
    ans <- many $ oneOf "10X"
    many $ string "\n"
    return $ Mask ans

program :: Parser([PLine])
program = do
    many $ string "\n"
    many (try maskLine <|> memLine)

readProg :: [Char] -> [PLine]
readProg = useParser program

binary36 :: Integer -> BinaryString
binary36 n = extra_zeros ++ bin_rep
    where bin_rep = binary n
          extra_zeros = take (36 - length bin_rep)$ repeat '0'

binary :: Integer -> String
binary 0 = ""
binary n = repr_s ++ digit
    where repr_s = binary (n `div` 2)
          digit = show (n `rem` 2)

-- |
-- >>> binary36 0
-- "000000000000000000000000000000000000"
    

-- >>> binary36 15
-- "000000000000000000000000000000001111"

applyMask :: BinaryString -> PLine -> [BinaryString]
applyMask bS (Mask mS) = b where
         a = zip bS mS
         b = foldr f [""] a
         f :: (Char, Char) -> [String] -> [String]
         f (c_bin,c_mask) list 
            | c_mask == '0' = map (c_bin :) list
            | c_mask == '1' = map ('1' :) list
            | c_mask == 'X' = map ('1' :) list ++ map ('0' :) list

-- |
-- >>> m = Mask "000000000000000000000000000000000000"
-- >>> s = binary36 11
-- >>> applyMask s m
-- ["000000000000000000000000000000001011"]
-- >>> m2 = Mask "000000000000000000000000000000010000"
-- >>> applyMask s m2
-- ["000000000000000000000000000000011011"]
-- >>> m3 = Mask "000000000000000000000000000000010X00"
-- >>> applyMask s m3
-- ["000000000000000000000000000000011111","000000000000000000000000000000011011"]


fromBinary :: String -> Integer
fromBinary "" = 0
fromBinary (c:cs) = curr+rest
    where rest = fromBinary cs
          curr = 2^length cs * read [c]

solve :: [PLine] -> Integer
solve l = sum $ map fromBinary $ map snd $ toList $ fst $ solve' (reverse l)

solve' :: [PLine] -> (Map Integer BinaryString, PLine)
solve' []         = (,) (fromList []) (Mask $ take 36 $ repeat 'X')
solve' (Mask mask:l) = (curr_map, Mask mask)
    where (curr_map,_) = solve' l
solve' ((ToMem addr val):l) = (updated_map,curr_mask)
    where (curr_map,curr_mask) = solve' l
          addr_with_mask = map read $ applyMask (binary36 addr) curr_mask :: [Integer]
          add_many :: BinaryString -> [Integer] -> Map Integer BinaryString -> Map Integer BinaryString
          add_many val addrs curr_map = foldr (\addr a_map -> insert addr val a_map) curr_map addrs
          updated_map = add_many (binary36 val) addr_with_mask curr_map 


main :: IO ()
main = interact ((++ "\n") . show . solve .readProg)

