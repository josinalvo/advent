import Common
import Data.Map (fromList, insert, toList, Map)

import Text.Parsec hiding(count, parse, uncons)


data PLine = ToMem {addr :: Integer, val :: Integer} | Mask String deriving Show
type BinaryString = String

memLine :: Parser (PLine)
memLine = do
    string "mem["
    addr <- many1 digit
    string "] = "
    val <- many1 digit
    many $ string "\n"
    return $ ToMem (read addr) (read val)

-- |
-- >>> parse memLine "mem[8] = 11"
-- Right (ToMem {addr = 8, val = 11})

maskLine :: Parser (PLine)
maskLine = do
    string "mask = "
    ans <- many $ oneOf "10X"
    many $ string "\n"
    return $ Mask ans

program :: Parser([PLine])
program = do
    many $ string "\n"
    many (try maskLine <|> memLine)

readProg :: [Char] -> [PLine]
readProg = useParser program

binary36 :: Integer -> BinaryString
binary36 n = extra_zeros ++ bin_rep
    where bin_rep = binary n
          extra_zeros = take (36 - length bin_rep)$ repeat '0'

binary :: Integer -> String
binary 0 = ""
binary n = repr_s ++ digit
    where repr_s = binary (n `div` 2)
          digit = show (n `rem` 2)

-- |
-- >>> binary36 0
-- "000000000000000000000000000000000000"
    

-- >>> binary36 15
-- "000000000000000000000000000000001111"

applyMask :: BinaryString -> PLine -> BinaryString
applyMask bS (Mask mS) = zipWith applyMask' bS mS
   where applyMask' c 'X' = c
         applyMask' _  c  = c

-- |
-- >>> m = Mask "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX0X"
-- >>> s = binary36 15
-- >>> applyMask s m
-- "000000000000000000000000000000001101"

fromBinary :: String -> Integer
fromBinary "" = 0
fromBinary (c:cs) = curr+rest
    where rest = fromBinary cs
          curr = 2^length cs * read [c]


-- |
-- >>> fromBinary "000000000000000000000000000000001101"
-- 13
-- >>> n = fromBinary "100000000000000000000000000000001101"
-- >>> 2^35+13 == n
-- True


solve :: [PLine] -> Integer
solve l = sum $ map fromBinary $ map snd $ toList $ fst $ solve' (reverse l)

solve' :: [PLine] -> (Map Integer BinaryString, PLine)
solve' []         = (,) (fromList []) (Mask $ take 36 $ repeat 'X')
solve' (Mask mask:l) = (curr_map, Mask mask)
    where (curr_map,_) = solve' l
solve' ((ToMem addr val):l) = (updated_map,curr_mask)
    where (curr_map,curr_mask) = solve' l
          val_with_mask = applyMask (binary36 val) curr_mask
          updated_map = insert addr val_with_mask curr_map


main :: IO ()
main = interact ((++ "\n") . show . solve .readProg)