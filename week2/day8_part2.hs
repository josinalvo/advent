import Common
import Data.Map(Map, insert, insertWith, delete, (!), (!?), adjust)


readMachine :: String -> [(String, Int)]
readMachine string = map breakCommand commandStrings
    where 
    commandStrings = lines string 
    breakCommand cString = (ope,num) 
        where [ope,b] = splitAtElem ' ' cString
              num = if b!!0 == '+'
                    then read (drop 1 b)
                    else read b
          
getCommandWithFlip commands pos shouldFlip = newcommand command shouldFlip
      where command = commands!!pos
            newcommand ("jmp",val) True = ("nop", val)
            newcommand ("nop",val) True = ("jump", val)
            newcommand tuple _ = tuple

runMachine :: Int -> Int -> [Int] -> [(String, Int)] -> Int  -> (Int,[Int])
runMachine pos acc history commands toFlip= if newpos `elem` history || pos == last
                                      then (newacc, history)
                                      else runMachine newpos newacc (newpos:history) commands toFlip
    where command = getCommandWithFlip commands pos (pos == toFlip)
          newpos_f ("jmp", val) = pos+val
          newpos_f (_,_) = pos+1
          newacc_f ("acc",val) = acc+val
          newacc_f (_,_) = acc
          (newpos,newacc) = (,) (newpos_f command) (newacc_f command)
          last = length commands - 1

runMachines commands = ans
      where last_flippable = length commands - 2
            last_command = length commands - 1
            runWithFlip = runMachine 0 0 [] commands
            allMachines = map runWithFlip [0..last_flippable] -- nao seria dificil pegar só as maquinas que faz sentido flipar, mas nao foi necessário (e não melhoraria o O grande)
            is_last_of_history r = (head . snd $ r) == last_command
            ans = head $ filter is_last_of_history allMachines



main :: IO ()
main = interact (show . runMachines . readMachine)  