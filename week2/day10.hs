import Data.List (sort, group, zipWith4)


differences :: [Int] -> [(Int,Int)]
differences list = condense
    where pairsList = zip list (drop 1 list)
          differList = group . sort . map (\(x,y) -> y-x) $ pairsList
          condense = map (\l -> (head l,length l)) differList

-- I want a better way to right condense. This is NOT better
-- a :: [[Int]]
-- a = map ([head,length] <*>) (group [[1,1],[2,2,2,2,2]])

cleanNums list = 0 : sorted ++ [last sorted+3]  
    where sorted = sort list          

--indicators list = [indicator(i `elem` list) | i <-[0..(maximum list)]]
--indicator x = if x then 1 else 0          
-- or maybe faster below
indicators list = reverse . indicators' . reverse. sort $ list 
    where zeroes = repeat 0
          indicators' [] = []
          indicators' (x:xs) = 1:fill ++ smaller
                               where smaller = indicators' xs
                                     fill = take (x - length smaller) zeroes
          



m list = zero:one:two:(zipWith4 f a (drop 1 a) (drop 2 a) (drop 3 $ indicators_list))
    where f a b c d = (a+b+c)*d
          a = m list
          indicators_list = indicators list
          zero = 1
          one = zero*indicators_list!!1
          two = (zero+one)*indicators_list!!2


readNums :: String -> [Int]
readNums = (map read) . lines

main1 = interact (show .  differences . cleanNums . readNums)

main = interact (show .  m . readNums)