import Common
import Data.List

input :: [Char] -> (Int, [Int])
input string = (read mytime,times_list)
    where [mytime,times] = lines string
          times_list =  map read (filter (/= "x") $ splitAtElem ',' times)

solve1 :: (Int,[Int]) -> Int
solve1 (mytime,times_list) = best_id * wait best_id
    where smallAfter = \x y -> compare (wait x)  (wait y)
          wait x = x - rem mytime x
          best_id = minimumBy smallAfter times_list
main = interact (show. solve1. input)