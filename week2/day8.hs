import Common
import Data.Map(Map, insert, insertWith, delete, (!), (!?), adjust)
import qualified Data.List

readMachine :: String -> [(String, Int)]
readMachine string = map breakCommand commandStrings
    where 
    commandStrings = lines string 
    breakCommand cString = (ope,num) 
        where [ope,b] = splitAtElem ' ' cString
              num = if b!!0 == '+'
                    then read (drop 1 b)
                    else read b
          
getCommandWithFlip commands pos _ = commands!!pos

runMachine :: Int -> Int -> [Int] -> [(String, Int)]  -> Int
runMachine pos acc history commands= if newpos `elem` history
                                      then newacc
                                      else runMachine newpos newacc (newpos:history) commands
    where command = getCommandWithFlip commands pos False
          newpos_f ("jmp", val) = pos+val
          newpos_f (_,_) = pos+1
          newacc_f ("acc",val) = acc+val
          newacc_f (_,_) = acc
          (newpos,newacc) = (,) (newpos_f command) (newacc_f command)




-- fringeByCost (cost->[(vertex,parent)])
-- fringeByVertex (vertex-> cost)


main :: IO ()
main = interact (show . (runMachine 0 0 []) . readMachine)  