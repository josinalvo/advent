getInput :: String -> [(Char, Int)]
getInput = (map sep) . lines
  where sep (d:rest) = (,) d (read rest)

-- data Direction = North | South | East | West deriving (Eq,Show)

-- dirs = [North,West,South, East]
-- char_dirs="NWSE"
-- instance Enum Direction where
--     toEnum n = dirs !! idx where idx = (n+4) `rem` 4
--     fromEnum dir = head [n | n<-[0..], dirs!!n == dir]

-- toChar :: Direction -> Char
-- toChar direction = char_dirs !! fromEnum direction

-- na empolgação perdi a maior parte do cod da parte 1

changePos :: (Int,Int) -> (Int,Int) -> (Char, Int) -> ((Int,Int),(Int,Int))
changePos (x,y) a ('N',amount) = (,) (x+amount,y) a
changePos (x,y) a ('S',amount) = (,) (x-amount,y) a
changePos (x,y) a ('E',amount) = (,) (x,y+amount) a
changePos (x,y) a ('W',amount) = (,) (x,y-amount) a
changePos (x,y) a ('R',amount) = (,) new a
    where quarters = amount `div` 90
          right (x,y) 0 = (x,y)
          right (x,y) quarters = right (r_once x y) (quarters-1)
          r_once x y = (-y,x)
          new = right (x,y) quarters
changePos (x,y) a ('L',amount) = (,) new a
    where quarters = amount `div` 90
          left (x,y) 0 = (x,y)
          left (x,y) quarters = left (l_once x y) (quarters-1)
          l_once x y = (y,-x)
          new = left (x,y) quarters
changePos (x,y) (x_ship, y_ship) ('F',amount) = (,) (x,y) (x_ship+grow_x,y_ship+grow_y) 
    where grow_x = x*amount
          grow_y = y*amount

go :: ((Int, Int),(Int,Int)) -> [(Char,Int)] -> ((Int, Int),(Int,Int))
go (point,ship) [] = (point,ship)
go (point,ship) (command:commands) = go  (changePos point ship command) commands

manhattan :: ((Int,Int),(Int,Int)) -> Int
manhattan (_,(x,y)) = abs x + abs y

main = interact (flip (++) "\n" . show . manhattan . go ((,) (1,10) (0,0)) . getInput)