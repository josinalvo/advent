
import Text.Parsec
import Text.Parsec.String (Parser)
import Text.Parsec.Char

import Debug.Trace

magic :: Parser a -> Parser b -> Parser String
magic side1 side2 = big_parser
    where ns = [1..100]
          parsers = [try ((count n side1) >> (count n side2) >> return "banana")| n <-ns ]
          big_parser = foldr1 (<|>) parsers

ab :: Parser String
ab = magic (char 'a') (char 'b')

-- |
-- >>> parse ab "nopes" "aabb"
-- Right ...
-- >>> parse ab "nopes" "aaabb"
-- Left ...
-- ...
-- >>> parse ab "nopes" "aaabbb"
-- Right ...

main = print 42