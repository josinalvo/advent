module Rot where

import Data.Char
import Data.List
import Data.Maybe

letters = ['a'..'z']

elemIndexJust target = fromJust . elemIndex target
-- curious still about not needing parens

rotateChar :: Int -> Char -> Char
rotateChar n l
  | isUpper l = toUpper $ rotateChar n $ toLower l
  | isAlpha l = toChar $ mod (toInt l + n) size
  | otherwise = l
  where size       = length letters
        toInt char = elemIndexJust char letters
        toChar n   = letters !! n 
-- |
-- >>> rotateChar 0 'a'
-- 'a'
-- >>> rotateChar 1 'B'
-- 'C'
-- >>> rotateChar 1 ' '
-- ' '

rotateStr :: Int -> [Char] -> [Char]
rotateStr n  = map (rotateChar n)

main = print 42