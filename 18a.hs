import Debug.Trace

apply :: String -> String -> String -> Int
apply ope x y
  | ope == "*" = (*) (read x) (read y)
  | ope == "+" = (+) (read x) (read y)

evalSimple1 :: [String] -> String
evalSimple1 [y] = y
evalSimple1 (x:ope:y:rest) = evalSimple1 $ (show $ apply ope x y):rest


evalSimpleSum :: [String] -> [String]
evalSimpleSum (x:ope:y:rest) | trace (show (x,ope,y, rest)) False = undefined 
evalSimpleSum [y] = [y]
evalSimpleSum (x:"*":rest) = x:"*":(evalSimpleSum rest)
evalSimpleSum (x:"+":y:rest) = evalSimpleSum $ (show $ apply "+" x y):rest


evalSimple2 :: [String] -> String
evalSimple2 opes = evalSimple1 summed
    where summed = evalSimpleSum opes

evalSimple = evalSimple2
-- $
-- >>> evalSimple1 $ words "1 + 2 * 3 + 4 * 5 + 6"
-- "71"

count c = length . filter (== c)
open = '('; close = ')'

parens' :: String -> Int -> [String] -> [String]
--parens' acc n a@(expr:exprs)| trace (show (acc,n,a)) False = undefined 
parens' _ 0 (expr:exprs) 
    | head expr == open = parens' remaining opens $ (:) clear exprs
    | otherwise = (:) expr (parens' "" 0 exprs)
        where opens = count open expr
              clear = filter (/= open) expr
              remaining = tail $ filter (== open) expr


parens' acc n (expr:exprs) 
    | (last expr == close && closes == n) = (:) (join acc $ init expr) (parens' "" 0 exprs)
    | last expr == close = parens' (join acc expr) (n-closes) exprs
    | head expr == open = parens' (join acc expr) (n+opens) exprs
    | otherwise = parens' (join acc expr) n exprs
        where opens = count open expr
              closes = count close expr

parens' _ _ [] = []

join s1 s2 
   | s1 == ""  = s2
   | last s1 == '(' = s1++s2
   | otherwise = s1 ++ " " ++ s2

onlyDigits :: String -> Bool
onlyDigits s = length others == 0
    where others = (filter (\c -> not $ elem c "0123456789")) s

eval :: String -> String
eval expString 
    | expString == "+" = "+"
    | expString == "*" = "*"
    | onlyDigits expString = expString
    | '(' `elem` expString = evalSimple $ map eval $ exps
    | otherwise = evalSimple (words expString)
        where
             exps :: [String] 
             exps = parens' "" 0 (words expString)

solveLines = show . sum . map (read . eval) . lines

main= interact solveLines