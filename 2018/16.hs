module PintoHahaha where

import Data.Bits
import Data.List
import System.IO  

{- HLINT ignore "Use camelCase" -}
operations :: [([Char], Int -> Int-> Int, (a, Int, Int, d) -> [Int] -> (Int, Int), [Char])]
operations = [
              ("addr", val_add, operands_rr,"ab"),
              ("addi", val_add, operands_ri,"a"),
              ("mulr", val_mul, operands_rr,"ab"),
              ("muli", val_mul, operands_ri,"a"),
              ("banr", val_ban, operands_rr,"ab"),
              ("bani", val_ban, operands_ri,"a"),
              ("borr", val_bor, operands_rr ,"ab"),
              ("bori", val_bor, operands_ri, "a"),
              ("setr", val_set, operands_ri,"a"),
              ("seti", val_set, operands_ii,""),
              ("gtrr", val_gt, operands_rr,"ab"),
              ("gtri", val_gt, operands_ri,"a"),
              ("gtir", val_gt, operands_ir,"b"),
              ("eqrr", val_eq, operands_rr,"ab"),
              ("eqri", val_eq, operands_ri,"a"),
              ("eqir", val_eq, operands_ir,"b")
              ]
-- |
-- >>> runOpe "addr" ("banana",3,2,0) [1,2,3,4] 
-- [7,2,3,4]
-- >>> runOpe "addi" ("banana",3,2,0) [1,2,3,4] 
-- [6,2,3,4]
-- >>> runOpe "mulr" ("banana",3,2,0) [1,2,3,4] 
-- [12,2,3,4]
-- >>> runOpe "muli" ("banana",3,2,0) [1,2,3,4] 
-- [8,2,3,4]


runOpe :: String -> (a, Int, Int, Int) -> [Int] -> [Int]
runOpe operation (opcode,a,b,c) vals = replacePos c vals val_to_place
    where (va,vb) = operands (opcode,a,b,c) vals
          (_,combiner, operands, _) = head $ filter ((== operation) . fst4 ) operations
          val_to_place = combiner va vb


fst4 :: (a, b, c, d) -> a
fst4 (x,_,_,_) = x

replacePos :: Int -> [a] -> a -> [a]
replacePos pos list val_to_place = start ++ [val_to_place] ++ end
    where start = take pos list
          end = drop (pos+1) list


operands_ii :: (a1, a2, b, d) -> p -> (a2, b)
operands_ii (opcode,a,b,c) vals = (a,b)

operands_rr :: (a, Int, Int, d) -> [b] -> (b, b)
operands_rr (opcode,a,b,c) vals = (vals!!a,vals!!b)
operands_ri :: (a1, Int, b, d) -> [a2] -> (a2, b)
operands_ri (opcode,a,b,c) vals = (vals!!a,b)

operands_ir :: (a1, a2, Int, d) -> [b] -> (a2, b)
operands_ir (opcode,a,b,c) vals = (a,vals!!b)

val_eq :: (Eq a, Num p) => a -> a -> p
val_eq a b = if a==b then 1 else 0
val_gt :: (Ord a, Num p) => a -> a -> p
val_gt a b = if a > b then 1 else 0
val_set :: p1 -> p2 -> p1
val_set a _ = a
val_bor :: Bits a => a -> a -> a
val_bor a b = a .|. b

val_ban :: Bits a => a -> a -> a
val_ban a b = a .&. b

val_mul :: Num a => a -> a -> a
val_mul a b = a*b

val_add :: Num a => a -> a -> a
val_add a b = a + b

len :: [a] -> Int
len = length



      
--todo checkMatches :: (Int, Int, Int, Int) -> [Int] -> [Int] -> Int
-- and make work with curry
checkMatches :: ((a, Int, Int, Int), [Int], [Int]) -> (a, [[Char]])
checkMatches ((opcode,a,b,c),vals1,vals2) = (opcode,filteredOps)
    where possibleOps = (map fst4) . (filter (not . tooBig)) $ operations
          tooBig (_,_,_,restr) = (a > 3 && 'a' `elem` restr) || (b > 3 && 'b' `elem` restr)
          applied = map (\ ope -> runOpe ope (opcode,a,b,c) vals1) possibleOps
          correct = filter ((== vals2) . fst) $ zip applied possibleOps
          filteredOps = map snd correct



readFromLines :: [[Char]] -> [((Int,Int,Int,Int),[Int],[Int])]
readFromLines [] = []
readFromLines ("":xs) = readFromLines xs
readFromLines (x:y:z:xs) = curr_tup : readFromLines xs
    where command = (map read) . words $ y 
          [opcode,a,b,c] = command
          curr_tup = (,,) (opcode,a,b,c) (memToList x) (memToList z)
          memToList = (map read) . (drop 1) . words . cleanMem
          cleanMem = filter (not . (`elem` "[],")) 

intersectLists :: [(Int,[String])] -> (Int,[String])
intersectLists = foldr1 (\ x y -> (fst x, intersect (snd x) (snd y)))

fstEq :: Eq a => (a, b1) -> (a, b2) -> Bool
fstEq x y = (fst x) == (fst y) 

-- aux for clear (therefore for deduce)
-- deletes extra ocurrences of an already deduced (opcode,command)
clear' :: Int -> String -> [(Int,[String])] -> [(Int,[String])]
clear' opcode command list = map remove list
    where remove (curr_opcode, commands)= if opcode /= curr_opcode
                                          then (curr_opcode,delete command commands)
                                          else (curr_opcode,commands)

-- aux for deduce
-- deletes extra occurencess for all already deduces (opcode, command)
clear :: [(Int,[String])] -> [(Int,[String])] -> [(Int,[String])]
clear [] all = all
clear (s:singles) all = clear' opcode command (clear singles all)
    where command = head $ snd s
          opcode = fst s

-- uses a process of exclusion to deduce (opcode,command) relation
deduce :: [(Int,[String])] -> [(Int,[String])]
deduce opcodes = if new_ops == opcodes
                 then new_ops
                 else deduce new_ops
    where singles = filter ((==1). length . snd) opcodes
          new_ops = clear singles opcodes

--runOpe operation (opcode,a,b,c) vals
execute :: [(Int,[String])] ->[(Int, Int, Int, Int)] -> [Int] -> [Int]
execute _ [] registrers = registrers
execute possibleCommands (c:commands) registrers = execute possibleCommands commands new_registrers
    where (ope_num,_,_,_) = c
          new_registrers =  runOpe ope_name c registrers
          ope_name = (head . snd . head) $ filter ((==ope_num) . fst) possibleCommands
main = do
    handle <- openFile "input_16_a" ReadMode  
    examples <- hGetContents handle
    handleb <- openFile "input_16_b" ReadMode  
    programString <- hGetContents handleb
    -- print $ ((map intersectLists) . (groupBy fstEq) . (map checkMatches) . readFromLines . lines) input
    let commandExamples = (readFromLines . lines) examples
        posRelationsCodeCommand = (map intersectLists) . (groupBy fstEq) . (sortOn fst) . (map checkMatches) $ commandExamples
        listOfCommands = deduce posRelationsCodeCommand
        result = execute listOfCommands program [0,0,0,0]
        program = (map (toIntTuple . words)) $ filter (/= "") $ lines programString
        toIntTuple [a,b,c,d] = (read a,read b,read c,read d)
    print posRelationsCodeCommand
    print ">--------<"
    print listOfCommands
    print result
    return (posRelationsCodeCommand,listOfCommands,result)
