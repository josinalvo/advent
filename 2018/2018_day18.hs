adjacents matrix (x,y) = [matrix!!ny!!nx | (nx,ny) <- index]
    where distance = 1
          range_delta = [(-distance)..distance]
          centered_index = [(i,j) | i<-range_delta, j<-range_delta, (i,j) /= (0,0)]
          index = [(x+i,y+j) | (i,j) <- centered_index , x+i >=0, x+i < sizex, y+j >=0, y+j < sizey]
          (sizex, sizey) = ((length . head) matrix,length matrix)

all_adj_reading_order matrix = map (adjacents matrix) index
    where (sizex, sizey) = ((length . head) matrix,length matrix)
          index = [(x,y)| y <- [0..(sizey-1)],x <- [0..(sizex-1)]]
matrix_pos m (x,y) = m!!y!!x
         
open = '.'
tree = '|'
lumberyard = '#'

iterate_pos (neigboors,symbol)
   | symbol == open = if (number tree) >= 3
                      then tree
                      else symbol
   | symbol == tree = if (number lumberyard) >= 3
                      then lumberyard
                      else symbol
   | symbol == lumberyard = if and [(number tree) >= 1,(number lumberyard) >= 1]
                            then symbol
                            else open
    where number a = (length . (filter (== a))) neigboors


iterate matrix = into_lines line_size new_matrix_in_a_line
    where neigs = all_adj_reading_order matrix
          flat = flatten matrix
          state = zip neigs flat
          new_matrix_in_a_line = map iterate_pos state
          line_size = length $ matrix!!0


flatten = foldr1 (++)

countElem a list = (length . (filter (== a))) list

into_lines _ [] = []
into_lines size list =  first : rest
        where first = take size list
              rest  = into_lines size (drop size list)

up_to_first_rep list = up_to_first' 0 list

up_to_first' n list = if (list!!n) `elem` (take n list)
                      then (take (n+1) list)
                      else up_to_first' (n+1) list

main = do
    input <- getContents
    let   matrix = lines input
          answers = matrix : map Main.iterate answers
          up_to = up_to_first_rep answers
          last_board = up_to!!(length up_to - 1)
          desired_index = 10^9
          repetitions =  filter ((==last_board) . fst) $ zip up_to [0..]
          first_rep_idx = snd . head $ repetitions
          second_rep_idx = snd . head . tail $ repetitions
          cycle_size = second_rep_idx - first_rep_idx
          desired_index_small = first_rep_idx + (desired_index - first_rep_idx) `rem` cycle_size
          vals = map val up_to
          val ans = product $ [countElem tree, countElem lumberyard] <*> [flatten ans]
    putStrLn input
    putStrLn "---------->"
    (putStrLn . show)  $ vals !! desired_index_small
    