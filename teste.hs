
import Text.Parsec
import Text.Parsec.String (Parser)

parse_one_or_more :: Parser String -> Parser String
parse_one_or_more p1 = (many1 (try p1)) >> eof >> return "bababa"

foolish_a = parse_one_or_more (try (string "aa") <|> string "aaa")

good_a = parse_one_or_more (string "aaa")

-- |
-- >>> parse foolish_a "unused triplet" "aaaaaaaaa"
-- Left...
-- ...
-- >>> parse good_a "unused" "aaaaaaaaa"
-- Right...

main = print 42
