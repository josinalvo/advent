module Get where

import Data.Array.Unboxed
import Control.Monad 
import Control.Monad.ST 
import Data.Array.ST
import Data.Char ( digitToInt )
import Data.List (sortOn)

import Data.Array

listToUArray :: [Int] -> UArray Int Int
listToUArray vals = runSTUArray $ do
        let end = length vals - 1
        myArray <- newArray (0,end) 0
        forM_ [0 .. end] $ \i -> do
            let val = vals !! i
            writeArray myArray i val
        return myArray

        

-- |
-- >>> listToUArray [10,22,3]
-- array (0,2) [(0,10),(1,22),(2,3)]

listToSTUArray :: [Int] -> STUArray s Int Int
listToSTUArray vals = do
        let end = length vals - 1
        myArray <- newArray (0,end) 0
        forM_ [0 .. end] $ \i -> do
            let val = vals !! i
            writeArray myArray i val
        myArray

main = print 42