-- sieve from stackoverflow https://stackoverflow.com/questions/8197032/starray-documentation-for-newbies-and-state-st-related-questions
-- sieve modified to not use unboxed arrays (afaik, unboxed would not support custom types)
-- experiment with 23 left unfinished

{-# LANGUAGE FlexibleContexts #-}

import Debug.Trace
import Control.Monad 
import Control.Monad.ST 
import Data.Array.ST
import Data.Char ( digitToInt )
import Data.List (sortOn)

import Data.Array
primesUpto :: Int -> [Int]
primesUpto n = [p | (p, True) <- assocs $ sieve n]

sieve :: Int -> Array Int Bool
sieve n = runSTArray $ do
    sieve <- newArray (2, n) True
    forM_ [2..n] $ \p -> do
        isPrime <- readArray sieve p
        when isPrime $ do
            forM_ [p*2, p*3 .. n] $ \k -> do
                writeArray sieve k False
    return sieve


example :: [Int]
example = map digitToInt "389125467"

input :: [Int]
input = map digitToInt "135468729"

input_full = fillup input

fillup :: (Num a, Enum a) => [a] -> [a]
fillup l = l ++ [10..10^6]

data Cup = Cup {label :: Int, nextPosition :: Int} deriving Show

type Start = Int

data Game = Game {cups :: (Array Int Cup), start :: Int}

instance Show Game
    where show g@(Game cups start) = showFst g ++ show' (step1 g)
           where show' g@(Game cups start_local) 
                  | (start_local == start) = ""
                  | otherwise = showFst g ++ show' (step1 g)
                 fst_cup (Game cups start)= cups ! start
                 showFst = show . label . fst_cup
                 step1 (Game c s) = Game c (nextPosition $ c ! s)

shift1 :: [a] -> [a]
shift1 list = take (length list) $ drop 1 $ cycle list

-- TODO probabily there is a better, non monadic, non iterative way to do init_cups
-- TODO use it
initGame :: [Int] -> Game
initGame labels = Game init_cups 1
      where init_cups = runSTArray $ do
                ans <- newArray (1,length labels) (Cup 0 0)
                forM_ orderedCups $ \cup -> do
                    writeArray ans (label cup) cup
                return ans
            next_addrs = shift1 labels
            listOfCups = zipWith Cup labels next_addrs
            orderedCups = sortOn label listOfCups

type Pos = Int
backwardFrom :: Cup -> Cup -> Cup -> Int -> Pos -> Int
backwardFrom r1 r2 r3 query max_label = if previous `elem` labels_removed
                    then backwardFrom r1 r2 r3 previous max_label
                    else previous
        where labels_removed = map label [r1,r2,r3]
              previous = backward1 query
              backward1 label = if label - 1 == 0
                                then max_label 
                                else label - 1

listOfChanges :: Cup -> Cup -> Int -> Cup -> Cup -> [(Int, Cup)]
-- listOfChanges insertAfter start new_start_pos r1 r3 | trace (show ("banana",insertAfter,start,new_start_pos,r1,r3)) False = undefined 
listOfChanges insertAfter start new_start_pos r1 r3 = zip (map positionOfCup reps) reps
    where 
        positionOfCup c = label c
        replacement_old_start = Cup (label start) new_start_pos
        replacement_r3 = Cup (label r3) (nextPosition insertAfter)
        replacement_insertAfter = Cup (label insertAfter) (positionOfCup r1)
        reps = [replacement_old_start, replacement_r3, replacement_insertAfter]

idShow :: (Show a, Show b) => a -> b -> b
--idShow s a | trace (show (s,a)) False = a
idShow s a = a -- WHYYYYY?

stepGame :: [Int] -> Int -> Array Int Cup
stepGame labels steps = runSTArray $ do list <- newArray (1,length labels) (Cup 0 0)
                                        forM_ orderedCups $ \cup -> do
                                                writeArray list (label cup) cup
                                        manySteps steps list start
                                        return list
    where next_addrs  = shift1 labels
          listOfCups  = zipWith Cup labels next_addrs
          orderedCups = sortOn label listOfCups
          size        = length labels
          start       = head labels
          --applyAlter alterations _ | trace (show ("alterations",alterations)) False = undefined
          applyAlter alterations list = do forM_ alterations $ \(pos,cup) -> (writeArray list (label cup) cup)
          manySteps 0 _ _ = return 12 -- TODO, pode ser undefined?
          manySteps steps list startP = do
              nextStart <- oneStep list startP
              manySteps (steps-1) list nextStart
          --oneStep _ startP | trace (show ("new start rec",startP)) False = undefined
          oneStep list startP = do  start <- readArray list startP
                                    let next c = readArray list (nextPosition c)
                                    r1 <- next start
                                    r2 <- next r1
                                    r3 <- next r2
                                    insertAfter <- readArray list (backwardFrom r1 r2 r3 (label start) size)
                                    let new_start_pos = (idShow "new_start sent") $ nextPosition r3
                                    let alterations = listOfChanges insertAfter start new_start_pos r1 r3
                                    applyAlter alterations list
                                    return new_start_pos

main :: IO ()
main = part2_in

      
part1_ex =  forM_ (zip [1..10] [3,3,7,3,9,7,8,7,5,5]) $ \(i,start) -> print $ (i,Game (stepGame example i) start)      
part1_ex2 = forM_ [99..101] $ \i -> print (Game (stepGame example i) 1)  
part1_in = print (Game (stepGame input 100) 1)
fillup_test = print (input_full !! (10^5)-3)

ans_part2 :: Array Int Cup -> Int
ans_part2 cups = (label cup2) * (label cup3)
    where cup1 = cups ! 1
          next c = cups ! nextPosition c
          (cup2,cup3) = (next cup1, next cup2) 
part2_in :: IO ()
part2_in = print $ ans_part2 (stepGame input_full $ 10^7)