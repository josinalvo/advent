import Data.Maybe ( fromJust )
import qualified Data.HashMap.Strict as D
type MType = D.HashMap Int Int

-- import qualified Data.IntMap as D
-- type MType = D.IntMap Int
-- Saves 10s out of 60s in versus a Map for 400*1000*30 steps
-- No big memory difference (around 4Gb)
-- HashMap saves another 10s, to a total of 40s, spends 4.7Gb
-- Not interested in writing imperative haskell yet, but I can get this to run on C with 0.6 s and 100mb

-- Ways to get lost: using too much memory
start = [20,0,1,11,6,3] :: [Int]
--start = [0,3,6] :: [Int]
startMap :: MType
startMap = D.fromList $ zip start [0..(length start - 2)]


addL :: (Int, MType, Int) -> (Int, MType, Int)
addL (last,m,size) = (,,) next (D.insert last lastP m) (size+1)
    where lastP = size - 1
          pos = D.lookup last m
          next 
            | pos == Nothing = 0
            | otherwise = fromJust pos

         
upTo :: Int -> b -> (b -> Int) -> (b -> b) -> b
upTo size obj sizeEval update = if size == sizeEval obj
                                then obj
                                else upTo size (update obj) sizeEval update

ans = (\(x,_,_) -> x) $
      upTo (30*1000*400) 
           (head $ reverse $ start, startMap, length start) 
           (\(_,_,z) -> z) 
           addL

test :: Integer -> Integer 
test n = sum [1..n]

-- main = interact (show . test . read)

main = print ans
