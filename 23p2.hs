-- altering from the other file. Will change this comment
-- will have to meld the other file back, deleted a lot

{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
import qualified Data.Vector  as V
import Data.List (sortOn)
import Data.Char ( digitToInt )
data Cup = Cup {label :: Int, nextPosition :: Int} deriving Show

type Start = Int

data Game = Game {cups :: (V.Vector Cup), start :: Int}

example :: [Int]
example = map digitToInt "389125467"

fillup :: (Num a, Enum a) => [a] -> [a]
fillup l = l ++ [10..10^6]

input :: [Int]
input = map digitToInt "135468729"

shift1 :: [a] -> [a]
shift1 list = take (length list) $ drop 1 $ cycle list

toCupVector :: [Int] -> Game
toCupVector labels = Game vecOfCups posFirst
    where next_addrs = map (+ negate 1) $ shift1 labels
          listOfCups = zipWith Cup labels next_addrs
          vecOfCups = V.fromList $ sortOn label listOfCups
          posFirst = label (head listOfCups) - 1

--alterFirst = 
--simple_test = alterfirst $ toCupVector example

