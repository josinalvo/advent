{- HLINT ignore "Use camelCase" -}

-- curiosamente, evilstar funciona mas star está dando pau
-- criar a representacao Rule foi essencial

import Text.Parsec
import Text.Parsec.String (Parser)
-- import Text.Parsec.Char
-- import Debug.Trace

import System.IO ( openFile, hGetContents, IOMode(ReadMode) )  


import qualified Data.IntMap as D
type MType = D.IntMap Rule




data Rule = And [Int]  | Or Rule Rule | Singleton Char | EvilStar Int Int | Star Int deriving(Show)



oneChar :: Parser Rule
oneChar = fmap Singleton $ do
               a <- between (char '"') (char '"') anyChar
               eof
               return a

orSeq :: Parser Rule
orSeq = do
    a <- simpleSeq
    char '|'
    b <- simpleSeq
    eof
    return $ Or a b

detectEvil :: Parser Rule
detectEvil = do
    a <- many1 digit
    string "**"
    b <- many1 digit
    eof 
    return $ EvilStar (read a) (read b)

detectStar :: Parser Rule
detectStar = do
    a <- many1 digit
    string "*"
    eof
    return $ Star (read a)


simpleSeq:: Parser Rule
simpleSeq = fmap And intList

intList :: Parser ([Int])
intList = do
          many (char ' ')
          a <- many1 digit
          rest <- (try intList) <|> (many (char ' ') >> return [])
          return $ (read a) : rest

parseRule :: Parser (Int,Rule)
parseRule = do
    pos <- many1 digit 
    char ':'
    many $ char ' '
    text <-  (try oneChar) <|>(try detectStar) <|> (try detectEvil) <|>  (try orSeq) <|> simpleSeq
    return (read pos, text)

unright (Right b) = b

parseRuleF = unright . (parse parseRule "ignored")

-- |
-- >>> parse parseRule "ignored" "3: \"a\""
-- Right (3,Singleton 'a')
-- >>> parse parseRule "ignored" "33: 44 55 9"
-- Right (33,And [44,55,9])
-- >>> parse parseRule "ignored" "131: 44 55 | 9"
-- Right (131,Or (And [44,55]) (And [9]))
-- >>> parseRuleF "131: 44 55 | 9"
-- (131,Or (And [44,55]) (And [9]))


isMatch (Right _) = True
isMatch (Left _) = False

ruleToParser :: MType -> Rule -> Parser String
ruleToParser _ (Singleton a) = string [a]
ruleToParser m (Or r1 r2) = parse_or (ruleToParser m r1) (ruleToParser m r2)
ruleToParser m (And l) = foldr1 parse_conc parsers
   where parsers = map toParser l
         toRule :: Int -> Rule
         toRule = (m D.!)
         toParser :: Int -> Parser String 
         toParser n = ruleToParser m $ toRule n
ruleToParser m (EvilStar a b) = toParser a b
    where toParser :: Int -> Int -> Parser String 
          toParser a b = parse_evilStar (ruleToParser m $ m D.! a) (ruleToParser m $ m D.! b)
ruleToParser m (Star a) = toParser a
    where toParser :: Int -> Parser String 
          toParser a = parse_star (ruleToParser m $ m D.! a)


parse_or :: Parser String -> Parser String -> Parser String 
parse_or p1 p2 = try p1 <|> p2
parse_conc p1 p2 = do
    a<-p1
    b<-p2
    return $ a++b 

-- TODO tentar de novo com a forma many1qq
parse_star :: Parser String -> Parser String
parse_star p1 = (many1 (try p1)) >> eof >> return "bababa"
-- parse_star p1 = big_parser
--     where ns = [1..200]
--           parsers = [try ((count n (try p1)) >> return "banana")| n <-ns ]
--           big_parser = foldr1 (<|>) parsers

parse_evilStar :: Parser a -> Parser b -> Parser String
parse_evilStar side1 side2 = big_parser
    where ns = [1..100]
          parsers = [try ((count n side1) >> (count n side2) >> return "banana")| n <-ns ]
          big_parser = foldr1 (<|>) parsers


run f_rules f_strings = do
    handle_r <- openFile f_rules ReadMode  
    rules_s <- hGetContents handle_r
    let rules = D.fromList $ map parseRuleF $ lines rules_s
        parser = (ruleToParser rules (rules D.! 0)) >> eof
    mapM_ print $ map show $ map (parse parseRule "unu rul") $ lines rules_s
    handle_s <- openFile f_strings ReadMode  
    strings <- hGetContents handle_s
    let l = map isMatch $ map (parse parser "unused banana") $ lines strings
        amount = length $ filter (==True) l
    -- print l
    print amount
    

big = run "19_rules" "19_strings"
small = run "19_small_rules" "19_small_strings"
main = big

