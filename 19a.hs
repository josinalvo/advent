{- HLINT ignore "Use camelCase" -}

import Text.Parsec
import Text.Parsec.String (Parser)
import Text.Parsec.Char

import Debug.Trace

import System.IO ( openFile, hGetContents, IOMode(ReadMode) )  


import qualified Data.IntMap as D
type MType = D.IntMap Rule




data Rule = And [Int]  | Or Rule Rule | Singleton Char deriving(Show)



oneChar :: Parser Rule
oneChar = fmap Singleton $ between (char '"') (char '"') anyChar

orSeq :: Parser Rule
orSeq = do
    a <- simpleSeq
    char '|'
    b <- simpleSeq
    return $ Or a b

simpleSeq:: Parser Rule
simpleSeq = fmap And intList

intList :: Parser ([Int])
intList = do
          many (char ' ')
          a <- many1 digit
          rest <- (try intList) <|> (many (char ' ') >> return [])
          return $ (read a) : rest

parseRule :: Parser (Int,Rule)
parseRule = do
    pos <- many1 digit 
    char ':'
    many $ char ' '
    text <- (try oneChar) <|> (try orSeq) <|> simpleSeq
    return (read pos, text)

unright (Right b) = b

parseRuleF = unright . (parse parseRule "ignored")

-- |
-- >>> parse parseRule "ignored" "3: \"a\""
-- Right (3,Singleton 'a')
-- >>> parse parseRule "ignored" "33: 44 55 9"
-- Right (33,And [44,55,9])
-- >>> parse parseRule "ignored" "131: 44 55 | 9"
-- Right (131,Or (And [44,55]) (And [9]))
-- >>> parseRuleF "131: 44 55 | 9"
-- (131,Or (And [44,55]) (And [9]))


isMatch (Right _) = True
isMatch (Left _) = False

ruleToParser :: MType -> Rule -> Parser String
ruleToParser _ (Singleton a) = string [a]
ruleToParser m (Or r1 r2) = parse_or (ruleToParser m r1) (ruleToParser m r2)
ruleToParser m (And l) = foldr1 parse_conc parsers
   where parsers = map toParser l
         toRule :: Int -> Rule
         toRule = (m D.!)
         toParser :: Int -> Parser String 
         toParser n = ruleToParser m $ toRule n

parse_or :: Parser String -> Parser String -> Parser String 
parse_or p1 p2 = try p1 <|> p2
parse_conc p1 p2 = do
    a<-p1
    b<-p2
    return $ a++b 


run f_rules f_strings = do
    handle_r <- openFile f_rules ReadMode  
    rules_s <- hGetContents handle_r
    let rules = D.fromList $ map parseRuleF $ lines rules_s
        parser = (ruleToParser rules (rules D.! 0)) >> eof
    mapM_ print $ map show $ map (parse parseRule "unu rul") $ lines rules_s
    handle_s <- openFile f_strings ReadMode  
    strings <- hGetContents handle_s
    let l = map isMatch $ map (parse parser "unused banana") $ lines strings
        amount = length $ filter (==True) l
    print l
    print amount
    

big = run "19_rules" "19_strings"
small = run "19_small_rules" "19_small_strings"
evil = run "19_evil_rules" "19_evil_strings"
main = evil

